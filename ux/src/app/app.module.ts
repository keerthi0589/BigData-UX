import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderFieldDataService} from './header-field-data.service';
import { InputDataService} from './flow/input-data.service';
import { FlowComponent } from './flow/flow.component';

@NgModule({
  declarations: [
    AppComponent,
    FlowComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [HeaderFieldDataService, InputDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
