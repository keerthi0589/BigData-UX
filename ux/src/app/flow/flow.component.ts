import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Subscription }   from 'rxjs/Subscription';
import { Http, Response } from '@angular/http';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';

import { HeaderFieldDataService } from '../header-field-data.service';
import { HeaderFieldData } from '../header-field-data';
import { InputDataService } from './input-data.service';
const URL = 'http://localhost:8080/api/flow-upload';

@Component({
  selector: 'app-flow',
  templateUrl: './flow.component.html',
  styleUrls: ['./flow.component.css']
})
export class FlowComponent implements OnInit {

	headerFieldData;
	subscription: Subscription;
  filters;
  aggregators;
	
  constructor(private headerFieldDataService: HeaderFieldDataService, private http: Http,
  	private el: ElementRef, private inputDataService: InputDataService) { 
    this.subscription = headerFieldDataService.headerFieldData$.subscribe(
    	data => {
    		this.headerFieldData = data;
    	}
    );
    this.filters = [{}];
    this.aggregators = [{}];
  }

  ngOnInit() {
  	
  }
  
  ngOnDestroy() {
  	this.subscription.unsubscribe();
  }

  updateFlowDetails(data: Array<HeaderFieldData>){
  	this.headerFieldData = data;
  }
  
  add(){
  	this.filters.push({});
  }
  
  remove(index){
  	//console.log(event);
  	this.filters.splice(index, 1);
  }


  addAgg(){
  	this.aggregators.push({});
  }
  
  removeAgg(index){
  	//console.log(event);
  	this.aggregators.splice(index, 1);
  }
  
  submitFlow()
  {
    //console.log(this.filters);
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#inputFile');
    
    let fileCount: number = inputEl.files.length;
    	if (fileCount > 0) { // a file was selected
        	this.inputDataService.uploadFile(inputEl.files.item(0), this.filters, this.aggregators);
        	
        	}
    
    }
}
