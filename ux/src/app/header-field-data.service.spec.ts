import { TestBed, inject } from '@angular/core/testing';

import { HeaderFieldDataService } from './header-field-data.service';

describe('HeaderFieldDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeaderFieldDataService]
    });
  });

  it('should be created', inject([HeaderFieldDataService], (service: HeaderFieldDataService) => {
    expect(service).toBeTruthy();
  }));
});
